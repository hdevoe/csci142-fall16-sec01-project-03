package controller;

import javax.swing.JOptionPane;

import model.CryptoModel;

/**
 * The controller class is responsible for the communication between the cryptomodel and view classes
 * @author hudson
 */
public class Controller {
	
	private CryptoModel myCryptoModel;
	private View myView;
	private JOptionPane Alert;
	
	/**
	 * Controller constructor makes a new cryptomodel and view
	 * @author hudson
	 */
	public Controller()
	{
		myCryptoModel = new CryptoModel();
		myView = new View(this);
	}
	
	/**
	 * The solveProblem function gets the strings written in the three textboxes in the view and creates a puzzle class with the given parameters 
	 * before returning the puzzleclass's solution to the problem if there is one
	 * @author hudson
	 */
	public void solveProblem()
	{
		myCryptoModel.setWord1(myView.getWord1());
		myCryptoModel.setWord2(myView.getWord2());
		myCryptoModel.setResultWord(myView.getResultWord());
		
		if (myCryptoModel.solvePuzzle())
		{
			myView.setWords(myCryptoModel.getWord1(), myCryptoModel.getWord2(), myCryptoModel.getResultWord());
			myView.setNumbers(myCryptoModel.getMyPuzzle().getAddend1(), myCryptoModel.getMyPuzzle().getAddend2(), myCryptoModel.getMyPuzzle().getSum());
		} else 
		{
		JOptionPane.showMessageDialog(Alert, "Unable to solve problem");
		clearProblem();
		}
		
		
	}
	/**
	 * Calls on the view to clear all text boxes
	 */
	public void clearProblem()
	{
		myView.clear();
	}
}
