package controller;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Frame;
import java.awt.GridLayout;
import java.lang.reflect.Method;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class View extends Frame
{
	
	//Properties
	private JPanel myPanel1;
	private JPanel myPanel2;
	private JTextField myWord1;
	private JTextField myWord2;
	private JTextField myResultWord;
	private JTextField myAddend1;
	private JTextField myAddend2;
	private JTextField mySum;
	
	private Button mySolveButton;
	private Button myClearButton;
	
	private ButtonListener mySolveListener;
	private ButtonListener myClearListener;
	
	private Controller myController;
	
	
	public View(Controller controller)
	{
		//Initialize Properties
		myWord1 = new JTextField(5);
		myWord2 = new JTextField(5);
		myResultWord = new JTextField(5);
		myAddend1 = new JTextField(5);
		myAddend2 = new JTextField(5);
		mySum = new JTextField(5);
		
		myPanel1 = new JPanel();
	
		this.setSize(400,200);
		this.setLayout(new BorderLayout());
		this.setTitle("Cryptarithmetic Solver");
		myPanel1.setLayout(new GridLayout(3,2));
		myPanel1.setSize(300,300);
		myPanel1.add(new JLabel("Word 1: "));
		myPanel1.add(myWord1);
		myPanel1.add(new JLabel("Addend 1: "));
		myPanel1.add(myAddend1);
		myPanel1.add(new JLabel("Word 2: "));
		myPanel1.add(myWord2);
		myPanel1.add(new JLabel("Addend 2: "));
		myPanel1.add(myAddend2);
		myPanel1.add(new JLabel("Result Word: "));
		myPanel1.add(myResultWord);
		myPanel1.add(new JLabel("Sum: "));
		myPanel1.add(mySum);
		
		myController = controller;
		
		this.associateListeners();
		
		mySolveButton = new Button("Solve");
		mySolveButton.setSize(100, 20);
		myClearButton = new Button("Clear");
		myClearButton.setSize(100, 20);
		
		mySolveButton.addMouseListener(mySolveListener);
		myClearButton.addMouseListener(myClearListener);
		
		myPanel2 = new JPanel();
		myPanel2.setLayout(new GridLayout(1,2));
		this.add(myPanel1, BorderLayout.CENTER);
		myPanel2.add(mySolveButton);
		myPanel2.add(myClearButton);
		this.add(myPanel2,BorderLayout.SOUTH);
		
		
		this.setLocationRelativeTo(null);
		this.setVisible(true);
		this.addWindowListener(new AWindowListener());		
	}
	
	public void associateListeners()
    {
        String error;
        Class<? extends Controller> controllerClass;
        Method solveMethod,
               clearMethod;
        Class<?>[] classArgs;
        Integer[] args;
        
        controllerClass = myController.getClass();
        
        error = null;
        solveMethod = null;
        clearMethod = null;

        classArgs = new Class[1];
        args = new Integer[1];
        
        // Set argument types for method invocations
        try
        {
           classArgs[0] = Class.forName("java.lang.Integer");
        }
        catch(ClassNotFoundException exception)
        {
           error = exception.toString();
           System.out.println(error);
        }
        
        // Associate method names with actual methods to be invoked
        try
        {
           solveMethod = controllerClass.getMethod("solveProblem");
           clearMethod = controllerClass.getMethod("clearProblem");        
        }
        catch(NoSuchMethodException exception)
        {
           error = exception.toString();
           System.out.println(error);
        }
        catch(SecurityException exception)
        {
           error = exception.toString();
           System.out.println(error);
        }
        
        // Set up listeners with actual argument values passed in
        // to methods
        mySolveListener = new ButtonListener(myController, solveMethod);
        myClearListener = new ButtonListener(myController, clearMethod);
    }
	
	/**
	 * Get Word 1 text from box
	 * @author hudson
	 * @return
	 */
	public String getWord1()
	{
		return myWord1.getText();
	}
	
	/**
	 * Get Word 2 text from box
	 * @author hudson
	 * @return
	 */
	public String getWord2()
	{
		return myWord2.getText();
	}
	
	public String getResultWord()
	{
		return myResultWord.getText();
	}
	
	public void clear()
	{
		myWord1.setText("");
		myWord2.setText("");
		myResultWord.setText("");
		myAddend1.setText("");
		myAddend2.setText("");
		mySum.setText("");
	}
	
	public void setNumbers(int addend1, int addend2, int sum)
	{
		myAddend1.setText(""+addend1);
		myAddend2.setText(""+addend2);
		mySum.setText(""+sum);
	}
		
	public void setWords(String word1, String word2, String result)
	{
		myWord1.setText(word1);
		myWord2.setText(word2);
		myResultWord.setText(result);
	}
}
