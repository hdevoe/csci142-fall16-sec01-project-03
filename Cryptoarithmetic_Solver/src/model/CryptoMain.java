package model;

import controller.Controller;

public class CryptoMain 
{

	private Controller myController;
	
	public static void main(String[] args)
	{
		new CryptoMain();
	}

	public CryptoMain()
	{
		setController(new Controller());
	}
	
	public void setController(Controller controller)
	{
		myController = controller;
	}
	
	public Controller getController()
	{
		return myController;
	}
}
