package model;

/**
 * The Puzzle class is responsible for the solving of the cryptarithmetic problem given from the CryptoModel class
 * @author hudson
 */
public class Puzzle 
{
	//variables
	private char[] myWord1;
	private char[] myWord2;
	private char[] myResultWord;
	private int[] myAddend1;
	private int[] myAddend2;
	private int[] mySum;
	private boolean myIsPuzzleSolved;
	private String myListOfLetters = " ";
	private int[] myUsedNumbers;
	
	//methods
	/**
	 * The puzzle constructor takes in a word1, word2, and result, calling on a solvePuzzleFunction to attempt to substitute the letters of the words
	 * with numbers
	 * @param word1
	 * @param word2
	 * @param result
	 */
	public Puzzle (String word1, String word2, String result)
	{
		
		if (word1 != null)setMyWord1(word1);
		if (word2 != null)setMyWord2(word2);
		if (result != null)setMyResultWord(result);
		
		setListOfLetters();
		myIsPuzzleSolved = false;
		
		if (word1 != null)myAddend1 = new int[myWord1.length];
		for (int i = 0; i < myAddend1.length; i++)
		{
			myAddend1[i] = -1;
		}
		
		if (word2 != null)myAddend2 = new int[myWord2.length];
		for (int i = 0; i < myAddend2.length; i++)
		{
			myAddend2[i] = -1;
		}
		
		if (result != null) mySum = new int[myResultWord.length];
		for (int i = 0; i < mySum.length; i++)
		{
			mySum[i] = -1;
		}
		
		setListOfLetters();
		solvePuzzle(myListOfLetters);

	}

	
	/**
	 * Runs through both addends and the sum to see if any digits remain -1, which would indicate that not all letters are assigned.
	 * @author hudson
	 * @return
	 */
	public boolean checkIfAllLettersAssigned()
	{
		for (int i = 0; i < myAddend1.length; i++)
		{
			if (myAddend1[i] == -1) return false;
		}
		
		for (int i = 0; i < myAddend2.length; i++)
		{
			if (myAddend2[i] == -1) return false;
		}
		
		for (int i = 0; i < mySum.length; i++)
		{
			if (mySum[i] == -1) return false;
		}
		return true;
	}
	
	/**
	 * Checks if all letters have been assigned and returns true if addend1 + addend2 = sum
	 * @author hudson
	 * @return
	 */
	public boolean solvePuzzle()
	{
		if (checkIfAllLettersAssigned())
		{
			if (getAddend1() + getAddend2() == getSum())
			{
				myIsPuzzleSolved = true;
				return myIsPuzzleSolved;
			} 
		}
		return false;
	}
	
	
	/**
	 * Recursively calls itself with a lettersLeft String, trying out every combination of numbers until the problem is solved or the problem cannot 
	 * be solved
	 * @param lettersLeft
	 * @return
	 */
	public boolean solvePuzzle(String lettersLeft)
	{
		if (lettersLeft.isEmpty())
		{
			if (getAddend1() + getAddend2() == getSum()) return true;
		} else 
		for (int i = 0; i <=9; i++)
		{
			if (!checkIfDigitUsed(i))
			{
				assignDigit(lettersLeft.charAt(0), i);
				if (solvePuzzle(lettersLeft.substring(1)))
				{
					return true;
				} else unassignDigit(lettersLeft.charAt(0), i);
			}
		}
		return false;
	}
	
	
	/**
	 * Returns true if puzzle solved
	 * @author hudson
	 * @return
	 */
	public boolean checkIfSolved()
	{
		return myIsPuzzleSolved;
	}
	
	/**
	 * Assigns digit for a given char; loops through each addend to equate the given number to the given letter 
	 * @author hudson
	 * @param letter
	 * @param digit
	 * @return
	 */
	public void assignDigit(char letter, int digit)
	{
		
		for (int i = 0; i < myListOfLetters.length(); i++)
		{
			if (myListOfLetters.charAt(i) == letter)
			{
				if (myUsedNumbers[i] == -1)
				{
					myUsedNumbers[i] = digit;
				}
			}
		}
		
		for (int i = 0; i < myWord1.length; i++)
		{
			if (myWord1[i] == letter)
			{
				if (myAddend1[i] == -1)
				{
					myAddend1[i] = digit;
				}
			}
		}
		
		for (int i = 0; i < myWord2.length; i++)
		{
			if (myWord2[i] == letter)
			{
				if (myAddend2[i] == -1)
				{
					myAddend2[i] = digit;
				}
			}
		}
		
		for (int i = 0; i < mySum.length; i++)
		{
			if (myResultWord[i] == letter)
			{
				if (mySum[i] == -1)
				{
					mySum[i] = digit;
				}
			}
		}
	}
	
	/**
	 * Unassigns the given digit from the given letter
	 * @author hudson
	 * @param letter
	 * @param digit
	 */
	public void unassignDigit(char letter, int digit)
	{
		for (int i = 0; i < myListOfLetters.length(); i++)
		{
			if (myListOfLetters.charAt(i) == letter)
			{
				if (myUsedNumbers[i] == digit)
				{
					myUsedNumbers[i] = -1;
					break;
				}
			}
		}
		
		for (int i = 0; i < myWord1.length; i++)
		{
			if (myWord1[i] == letter)
			{
				if (myAddend1[i] == digit)
				{
					myAddend1[i] = -1;
				}
			}
		}
		
		for (int i = 0; i < myWord2.length; i++)
		{
			if (myWord2[i] == letter)
			{
				if (myAddend2[i] == digit)
				{
					myAddend2[i] = -1;
				}
			}
		}
		
		for (int i = 0; i < mySum.length; i++)
		{
			if (myResultWord[i] == letter)
			{
				if (mySum[i] == digit)
				{
					mySum[i] = -1;
				}
			}
		}
	}
	
	/**
	 * Checks to see if a digit has been used in the problem
	 * @author hudson
	 * @param digit
	 * @return
	 */
	public boolean checkIfDigitUsed(int digit)
	{
		for (int i = 0; i < myUsedNumbers.length; i++)
		{
			if (myUsedNumbers[i] == digit) return true;
		}
		return false;
	}
	
	/**
	 * Set word 1 char array to equal the string word1
	 * @author hudson
	 * @param word1
	 */
	private void setMyWord1(String word1)
	{
		myWord1 = new char[word1.length()];
		for (int i = 0; i < myWord1.length;i++)
		{
			myWord1[i] = word1.charAt(i);
		}
	}
	
	/**
	 * Set word2 char array to equal the string word2
	 * @author hudson
	 * @param word2
	 */
	public void setMyWord2 (String word2)
	{
		myWord2 = new char[word2.length()];
		for (int i = 0; i < myWord2.length;i++)
		{
			myWord2[i] = word2.charAt(i);
		}
	}
	
	/**
	 * Set resultWord char array to equal the string result
	 * @author hudson
	 * @param result
	 */
	public void setMyResultWord(String result)
	{
		myResultWord = new char[result.length()];
		for (int i = 0; i < myResultWord.length;i++)
		{
			myResultWord[i] = result.charAt(i);
		}
	}
	
	/**
	 * Set LetterList string to equal all unique chars found in the word1, word2, and myResultWord char arrays
	 * @author hudson
	 */
	private void setListOfLetters()
	{
		//Create list of letters
		for (int i = 0; i < myWord1.length; i++)
		{
			for (int t = 0; t < myListOfLetters.length(); t ++)
			{
				if (myListOfLetters.charAt(t) == myWord1[i])
				{
					break;
				}
						
				if (myListOfLetters == " ")
				{
					myListOfLetters = Character.toString(myWord1[i]);
					break;
				}
				
				if (t == myListOfLetters.length() - 1)
				{
					myListOfLetters += myWord1[i];
				}
			}
		}
				
		for (int i = 0; i < myWord2.length; i++)
		{
			for (int t = 0; t < myListOfLetters.length(); t ++)
			{
				if (myListOfLetters.charAt(t) == myWord2[i])
				{
					break;
				}
						
				if (t == myListOfLetters.length() - 1)
				{
					myListOfLetters += myWord2[i];
				}
			}
		}
				
		for (int i = 0; i < myResultWord.length; i++)
		{
			for (int t = 0; t < myListOfLetters.length(); t ++)
			{
				if (myListOfLetters.charAt(t) == myResultWord[i])
				{
					break;
				}
						
				if (t == myListOfLetters.length() - 1)
				{
					myListOfLetters += myResultWord[i];
				}
			}	
		}
		
		myUsedNumbers = new int[myListOfLetters.length()];
		for (int i = 0; i < myUsedNumbers.length; i++)
		{
			myUsedNumbers[i] = -1;
		}
	}
	
	/**
	 * Get String of Letters
	 * @author hudson
	 * @return
	 */
	public String getListOfLetters()
	{
		return myListOfLetters;
	}
	
	/**
	 * Return addend1
	 */
	public int getAddend1()
	{
		int addend1 = 0;
		
		for (int i = myAddend1.length-1; i >= 0 ; i--)
		{
			int multiplier = 1;
			for (int t = 0; t < myAddend1.length - (i+1); t++)
			{
				multiplier *= 10;
			}
			
			addend1 += (myAddend1[i] * multiplier);
		}
		return addend1;
	}
	
	/**
	 * Return addend2
	 */
	public int getAddend2()
	{
		int addend2 = 0;
		
		for (int i = myAddend2.length-1; i >= 0 ; i--)
		{
			int multiplier = 1;
			for (int t = 0; t <  myAddend2.length - (i+1); t++)
			{
				multiplier *= 10;
			}
			
			addend2 += (myAddend2[i] * multiplier);
		}
		
		return addend2;
	}
	
	/**
	 * Return sum
	 */
	public int getSum()
	{
		int sum = 0;
		
		for (int i = mySum.length-1; i >= 0 ; i--)
		{
			int multiplier = 1;
			for (int t = 0; t <  mySum.length - (i+1); t++)
			{
				multiplier *= 10;
			}
			
			sum += (mySum[i] * multiplier);
		}
		
		return sum;
	}
}
