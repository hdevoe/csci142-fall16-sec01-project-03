package model;

import javax.swing.JOptionPane;

/**
 * The cryptomodel is responsible for handling the relationship between the puzzle and the controller classes
 * @author hudson
 *
 */
public class CryptoModel {
	
	private Puzzle myPuzzle;
	private String myWord1;
	private String myWord2;
	private String myResultWord;
	private JOptionPane Alert;
	private static final int MAX_LENGTH = 10;
	
	/**
	 * Creates new null CryptoModel with an empty puzzle
	 */
	public CryptoModel()
	{
		myWord1 = null;
		myWord2 = null;
		myResultWord = null;
	}
	
	/**
	 * puts words to caps
	 * @param word
	 * @return
	 */
	private String toCaps(String word)
	{
		return word.toUpperCase();
	}
	
	/**
	 * Checks if word1, word2, and resultWord are not null, that result word is not 2 positions greater than both sums, that they are all less than
	 * MAX_LENGTH, and that they only contain letters
	 * @author hudson
	 * @param word1
	 * @param word2
	 * @param resultWord
	 * @return
	 */
	private boolean checkValid(String word1, String word2, String resultWord)
	{
		
		if (word1 == null || word2 == null || resultWord == null || word1 == " " || word2 == " " || resultWord == " ") 
		{
			JOptionPane.showMessageDialog(Alert, "Invalid Input: \n Empty addend");
			return false;
		}
		
		if (word1.length() < resultWord.length()-1 && word2.length() < resultWord.length()-1) 
		{
			JOptionPane.showMessageDialog(Alert, "Invalid Input: \n Result too long compared to addends");
			return false;
		}
		
		if (word1.length() > MAX_LENGTH || word2.length() > MAX_LENGTH || resultWord.length() > MAX_LENGTH)
		{
			JOptionPane.showMessageDialog(Alert, "Invalid Input: \n Exceeds maximum length (" + MAX_LENGTH + ")");
			return false;
		}
		
		if (word1.length() == 0|| word2.length() == 0 || resultWord.length() == 0)
		{
			JOptionPane.showMessageDialog(Alert, "Invalid Input: \n Empty addend");
			return false;
		}

		for (int i = 0; i < word1.length(); i++) 
		{
			if (!Character.isLetter(word1.charAt(i)))
			{
				JOptionPane.showMessageDialog(Alert, "Invalid Input: \n Addend 1 contains invalid figures");
				return false;
			}
		}
		
		for (int i = 0; i < word2.length(); i++) 
		{
			if (!Character.isLetter(word2.charAt(i)))
			{
				JOptionPane.showMessageDialog(Alert, "Invalid Input: \n Addend 2 contains invalid figures");
				return false;
			}
		}
		
		for (int i = 0; i < resultWord.length(); i++) 
		{
			if (!Character.isLetter(resultWord.charAt(i)))
			{
				JOptionPane.showMessageDialog(Alert, "Invalid Input: \n Sum contians invalid figures");
				return false;
			}
		}
		
		
		return true;
	}
	/**
	 * Sets word1
	 * @author hudson
	 * @param word1
	 */
	public void setWord1(String word1)
	{
		myWord1 = toCaps(word1);
	}
	
	/**
	 * Sets word2
	 * @author hudson
	 * @param word2
	 */
	public void setWord2(String word2)
	{
		myWord2 = toCaps(word2);
	}
	
	/**
	 * Sets result word
	 * @param result
	 */
	public void setResultWord(String result)
	{
		myResultWord = toCaps(result);
	}
	
	/**
	 * Get word1
	 * @author hudson
	 * @return
	 */
	public String getWord1()
	{
		return myWord1;
	}
	
	/**
	 * Get word2
	 * @author hudson
	 * @return
	 */
	public String getWord2()
	{
		return myWord2;
	}
	
	/**
	 * Get result word
	 * @author hudson
	 * @return
	 */
	public String getResultWord()
	{
		return myResultWord;
	}
	
	/**
	 * If myWord1, myWord2, and myResultWord are valid, solve puzzle and return true, else return false
	 */
	public boolean solvePuzzle()
	{
		if (checkValid(myWord1, myWord2, myResultWord))
		{
			myPuzzle = new Puzzle(myWord1, myWord2, myResultWord);
			if (myPuzzle.solvePuzzle())
			{
				return true;
			}
		}
		return false;
	}
	/**
	 * returns myPuzzle
	 * @author hudson
	 * @return
	 */
	public Puzzle getMyPuzzle()
	{
		return myPuzzle;
	}
	
}
